package jp.co.uniquevision.cooperatewithcamera;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements View.OnClickListener {

        private static final int CAPTURE_IMAGE_REQUEST_CODE = 1;

        private Button mStartCameraButton;
        private ImageView mResultImageView;

        private Bitmap mBitmap;
        private Uri mFileUri;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            mStartCameraButton = (Button) rootView.findViewById(R.id.start_camera);
            mResultImageView = (ImageView) rootView.findViewById(R.id.result);

            mStartCameraButton.setOnClickListener(this);

            return rootView;
        }

        @Override
        public void onClick(View view) {
            if (!isMediaMounted()) {
                Toast.makeText(getActivity(), "media not mounted", Toast.LENGTH_SHORT);
                return;
            }

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            mFileUri = getOutputMediaFileUri();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mFileUri);

            startActivityForResult(intent, CAPTURE_IMAGE_REQUEST_CODE);
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == CAPTURE_IMAGE_REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if (mBitmap != null) {
                            mBitmap.recycle();
                        }

                        // TODO `mFileUri`に画像を保存してくれないカメラアプリへの対応
                        // Galaxy S3とか
                        InputStream stream = getActivity().getContentResolver()
                                .openInputStream(mFileUri);
                        mBitmap = BitmapFactory.decodeStream(stream);
                        stream.close();
                        mResultImageView.setImageBitmap(mBitmap);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // user cancelled the image capture
                } else {
                    // image capture failed
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        }

        private static Uri getOutputMediaFileUri() {
            return Uri.fromFile(getOutputMediaFile());
        }

        private static boolean isMediaMounted() {
            return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
        }

        private static File getOutputMediaFile() {
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "CooperateWithCamera");

            // まだない場合ストレージディレクトリを作成する
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("CooperateWithCamera", "failed to create directory");
                    return null;
                }
            }

            // メディアファイル名の作成
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File mediaFile = new File(mediaStorageDir.getPath() +
                    File.separator + "IMG_" + timeStamp + ".jpg");

            return mediaFile;
        }
    }

}
